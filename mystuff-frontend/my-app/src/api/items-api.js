import config from '../environment/config'
import fetch from 'node-fetch'

class ItemsApi {

    async getItems(searchText) {
        const params = {
            keywords: searchText.replace(' ', ',')
        }

        const url = `${config.api_items_get_items}/?${queryParams(params)}`
        const res = await fetch(url, {method: 'GET'})
        const json = await res.json()
        if(json.status === 'success') return json.message.items
        else throw Error(json.message)
    }

    getItem = async token => {
        const url = `${config.api_items_get_item}/${token}`
        const res = await fetch(url, {method: 'GET'})
        const json = await res.json()
        
        if (json.status === 'success') return json.message.item
        else throw Error(json.message)
    }

    getUserItems = async uid => {
        const url = `${config.api_items_get_user_items}/${uid}`
        const res = await fetch(url, {method: 'GET'})
        const json = await res.json()

        if(json.status === 'success') return json.message.items
        else throw Error(json.message)
    }

    async getAllCategories() {
        let url = `${config.api_items_get_categories}`
        const res = await fetch(url, {method: 'GET'})
        const json = await res.json()

        if(json.status === 'success') return json.message.categories
        else throw Error(json.message)
    }

    async getAllConditions() {
        let url = `${config.api_items_get_conditions}`
        const res = await fetch(url, {method: 'GET'})
        const json = await res.json()

        if(json.status === 'success') return json.message.conditions
        else throw Error(json.message)
    }

    async postNewitem(item) {
        let url = `${config.api_items_post_new}`
        const res = await fetch(url,{
            method: 'POST',
            body: JSON.stringify(item),
            headers: {'Content-Type': 'application/json'}
        })
        const json = await res.json()

        if(json.status === 'success') return json.message.item
        else throw Error(json.message)
    }

    setPublished = async (token, published) => {
        let url = `${config.api_items_put_published}`
        const res = await fetch(url, {method: 'PUT', body: JSON.stringify({token: token, published: published}), headers: {'Content-type': 'application/json'}})
        const json = await res.json()

        if(json.status !== 'success') throw Error(json.message)
    }

}

function queryParams(params) {
	return Object.keys(params)
		.map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
		.join('&')
}

export default ItemsApi