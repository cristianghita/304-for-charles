import config from '../environment/config'
const fetch = require('node-fetch')

class UsersApi {
    
    async isUsernameAvailable(username) {
        let url = `${config.api_users_get_isUsernameAvailable}/${username}`
        const res = await fetch(url, { method: 'GET' }) 
        const json = await res.json()
        
        if(json.status === 'success') return json.message.available
        else throw Error(json.message)
    }

    getUserSavedItems = async uid => {
        let url = `${config.api_users_get_savedItems}/${uid}`
        const res = await fetch(url, {method: 'GET'})
        const json = await res.json()

        if(json.status === 'success') return json.message.items
        else throw Error(json.message)
    }

    async getUser(uid) {
        let url = `${config.api_users_get_user}/${uid}`
        const res = await fetch(url, {method: 'GET'})
        const json = await res.json()

        if(json.status === 'success') return json.message.user
        else throw Error(json.message)
    }

    getUserChats = async uid => {
        let url = `${config.api_users_get_userChats}/${uid}`
        const res = await fetch(url, {method: 'GET'})
        const json = await res.json()

        if(json.status === 'success') return json.message.chats
        else throw Error(json.message)
    }

    getChat = async chatToken => {
        let url = `${config.api_users_get_chat}/${chatToken}`
        const res = await fetch(url, {method: 'GET'})
        const json = await res.json()

        if(json.status === 'success') return json.message.chat
        else throw Error(json.message)
    }

    getReportedUsers = async () => {
        let url = config.api_users_get_reported
        const res = await fetch(url, {method: 'GET'})
        const json = await res.json()

        if(json.status === 'success') return json.message.reportedUsers
        else throw Error(json.message)
    }

    async postNewUser(user) {
        let url = config.api_users_post_newUser
        const res = await fetch(url, {
            method: 'POST',
            body: JSON.stringify(user),
            headers: {'Content-Type': 'application/json'}
        })
        const json = await res.json()
        
        if(json.status === 'success') return json.message.user
        else throw Error(json.message)
    }

    /**
     * Send a message to a user
     *
     * @param {*} message {uidFrom, uidTo, message, chatToken, realtedItemToken}
     * @returns message+chat object
     */
    sendMessage = async message => {
        let url = config.api_users_post_message
        const res = await fetch(url, {
            method: 'POST',
            body: JSON.stringify(message),
            headers: {'Content-Type': 'application/json'}
        })
        const json = await res.json()

        if(json.status === 'success') return json.message.message
        else throw Error(json.message)
    }

    reportUser = async (uidReporter, uidReported, comment) => {
        const url = config.api_users_post_report
        const res = await fetch(url, {
            method: 'POST', 
            body: JSON.stringify({uidReporter: uidReporter, uidReported: uidReported, comment: comment}),
            headers: {'Content-Type': 'application/json'}
        })
        const json = await res.json()

        if(json.status === 'success') return json.message.reportUser
        else throw Error(json.message)
    }

    async saveUnsaveItem(token, save, uid) {
        let url = `${config.api_users_put_saveUnsaveItem}`
        const res = await fetch(url, {method: 'PUT', body: JSON.stringify({token: token, save: save, uid: uid}), headers: {'Content-Type': 'application/json'}})
        const json = await res.json()

        if(json.status === 'success') return json.message.saveUnsaveItem
        else throw Error(json.message)
    }

    setRating = async (uidFrom, uidTo, rating) => {
        let url = `${config.api_users_put_setRating}`
        const res = await fetch(url, {method: 'PUT', body: JSON.stringify({uidTo: uidTo, uidFrom: uidFrom, rating: rating}), headers: {'Content-Type': 'application/json'}})
        const json = await res.json()

        if(json.status === 'success') return json.message.rating
        else throw Error(json.message)
    }

}

export default UsersApi