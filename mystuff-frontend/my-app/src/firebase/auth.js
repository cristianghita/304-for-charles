import { auth } from './firebase'

// Sign Up
export const registerWithEmail = async (email, password) =>
    await auth.createUserWithEmailAndPassword(email, password) 
    
// Sign In
export const signInWithEmail = async (email, password) => 
    await auth.signInWithEmailAndPassword(email, password)
    
// Sign Out
export const doSignOut = async () => 
    await auth.signOut()
    
// Password Reset
export const resetPassword = async (email) =>
  await auth.sendPasswordResetEmail(email);

// Password Change
export const updatePassword = async (password) =>
  await auth.currentUser.updatePassword(password);

