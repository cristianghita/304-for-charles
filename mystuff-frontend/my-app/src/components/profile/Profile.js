import React, { Component } from 'react'
import './Profile.css'
import { withRouter } from 'react-router-dom'
import UsersApi from '../../api/users-api'
import FontAwesome from 'react-fontawesome'
import { UserItems } from '../account/Account'

const usersApi = new UsersApi()

class ProfilePage extends Component {
    constructor(props) {
        super(props)

        this.state = {user: null, items: [], hover: 0, currentUserRating: null, currentUserRatingUpdated: false, totalRating: {count: 0, average: 0}, showReportSection: false}
    }

    /**
     * Gets the user object of the profile owner
     * and updates his rating if existing
     */
    componentDidMount = async () => {
        const user = await usersApi.getUser(this.props.match.params.uid)
        this.setState({user: user})
        if(user.ratings === undefined) return
        this.updateTotalRating()
    }

    /**
     * Updates local state with count of ratings
     * and average of this ratings
     */
    updateTotalRating = async () => {
        let totalRating = {count: 0, average: 0}
        const ratings = this.state.user.ratings
        if (ratings !== undefined) {
            const count = Object.keys(ratings).length
            let sum = 0
            for (const token in ratings) sum += ratings[token]
            const average = sum / count

            totalRating = {count: count, average: average}
        }
        this.setState({totalRating: totalRating})
    }

    /**
     * Updates rating to display logged in user rating of 
     * opened profile
     */
    visualUpdateUserRating = async () => {
        if(this.state.user.ratings === undefined) return
        if(this.state.user.ratings[this.props.authUser.uid] !== undefined) this.setState({currentUserRating: this.state.user.ratings[this.props.authUser.uid]})
    }

    /**
     * Sets the rating of logged in user for the opened profile
     *
     * @param {*} rating
     */
    rateUser = async rating => {
        if(this.state.currentUserRating === rating) rating = null
        this.setState({currentUserRating: rating})
        await usersApi.setRating(this.props.authUser.uid, this.state.user.uid, rating)
        this.setState({user: await usersApi.getUser(this.props.match.params.uid)})
        this.updateTotalRating()
    }

    render() {
        const defaultStarColor = 'rgb(218, 223, 225)'
        const hoverStarColor = 'rgb(255, 227, 71)'
        const selectedStarColor = 'rgb(255, 174, 0)'

        if(this.props.authUser && !this.state.currentUserRatingUpdated && this.state.user) {
            this.setState({currentUserRatingUpdated: true})
            this.visualUpdateUserRating()
        }

        // disable rating stars if user is same as profile or not logged in
        const userCanVote = this.props.authUser && this.state.user && this.props.authUser.uid !== this.state.user.uid

        return(
            <div className='profile-page'>
                {this.state.user === null ? null : 
                    <div>
                        <span className='info'><b>Username: </b>{this.state.user.username}</span> <br/> 
                        <span className='info'><b>Rating: </b>{this.state.totalRating.average} <i>({this.state.totalRating.count} votes)</i></span> <br/> 
                        {!userCanVote ? null : 
                        <div>
                            <div className='rating-stars'>
                                <FontAwesome name='fas fa-star' className='star' size="2x" onMouseEnter={() => this.setState(byPropKey('hover', 1))} onMouseLeave={() => this.setState(byPropKey('hover', 0))}
                                    style={{color: 
                                        this.state.hover !== 0
                                        ? ( this.state.hover >= 1 ? hoverStarColor : defaultStarColor )
                                        : ( this.state.currentUserRating
                                            ? ( this.state.currentUserRating >= 1 
                                                ? selectedStarColor
                                                : defaultStarColor) 
                                            : defaultStarColor )}}
                                onClick={() => this.rateUser(1)}/>
                                <FontAwesome name='fas fa-star' className='star' size="2x" onMouseEnter={() => this.setState(byPropKey('hover', 2))} onMouseLeave={() => this.setState(byPropKey('hover', 0))}
                                style={{color: 
                                    this.state.hover !== 0
                                    ? ( this.state.hover >= 2 ? hoverStarColor : defaultStarColor )
                                    : ( this.state.currentUserRating
                                        ? ( this.state.currentUserRating >= 2 
                                            ? selectedStarColor
                                            : defaultStarColor) 
                                        : defaultStarColor )}}
                                onClick={() => this.rateUser(2)}/>
                                <FontAwesome name='fas fa-star' className='star' size="2x" onMouseEnter={() => this.setState(byPropKey('hover', 3))} onMouseLeave={() => this.setState(byPropKey('hover', 0))}
                                style={{color: 
                                    this.state.hover !== 0
                                    ? ( this.state.hover >= 3 ? hoverStarColor : defaultStarColor )
                                    : ( this.state.currentUserRating
                                        ? ( this.state.currentUserRating >= 3 
                                            ? selectedStarColor
                                            : defaultStarColor) 
                                        : defaultStarColor )}}
                                onClick={() => this.rateUser(3)}/>
                                <FontAwesome name='fas fa-star' className='star' size="2x" onMouseEnter={() => this.setState(byPropKey('hover', 4))} onMouseLeave={() => this.setState(byPropKey('hover', 0))}
                                style={{color: 
                                    this.state.hover !== 0
                                    ? ( this.state.hover >= 4 ? hoverStarColor : defaultStarColor )
                                    : ( this.state.currentUserRating
                                        ? ( this.state.currentUserRating >= 4 
                                            ? selectedStarColor
                                            : defaultStarColor) 
                                        : defaultStarColor )}}
                                onClick={() => this.rateUser(4)}/>
                                <FontAwesome name='fas fa-star' className='star' size="2x" onMouseEnter={() => this.setState(byPropKey('hover', 5))} onMouseLeave={() => this.setState(byPropKey('hover', 0))}
                                style={{color: 
                                    this.state.hover !== 0
                                    ? ( this.state.hover >= 5 ? hoverStarColor : defaultStarColor )
                                    : ( this.state.currentUserRating
                                        ? ( this.state.currentUserRating >= 5 
                                            ? selectedStarColor
                                            : defaultStarColor) 
                                        : defaultStarColor )}}
                                onClick={() => this.rateUser(5)}/>
                            </div>
                            <button className='report-button' onClick={() => this.setState(byPropKey('showReportSection', !this.state.showReportSection))}>Report user</button>
                            {!this.state.showReportSection ? null : 
                                <ReportSection {...this.props} authUser={this.props.authUser} profileUid={this.state.user.uid}/>
                            }
                        </div>
                        }
                        <div style={{textAlign: 'left'}}><UserItems authUser={{uid: this.state.user.uid}} history={this.props.history}
                        differentUser={this.props.authUser ? (this.props.authUser.uid !== this.state.user.uid) : true}/></div>
                    </div>
                }
                
                
            </div>
        )
    }
}

class ReportSection extends Component {
    constructor(props) {
        super(props)

        this.state = {comment: '', responseMessage: null, buttonSendDisabled: false} //'Report sent successfully ✔'
    }

    /**
     * Reports opened profile to admins
     *
     */
    sendReport = async () => {
        this.setState({buttonSendDisabled: true})
        await usersApi.reportUser(this.props.authUser.uid, this.props.profileUid, this.state.comment)
        this.setState({comment: '', responseMessage: 'Report sent successfully ✔'})
    }

    render() {
        const buttonSendDisabled = this.state.comment === '' || this.state.buttonSendDisabled

        return(
            <div className='report-area'>
                {this.state.responseMessage ? <span className='response-message'>{this.state.responseMessage}</span> : 
                    <div>
                        <textarea placeholder='Give us more details..' className='report-textarea' autoFocus
                        value={this.state.comment} onChange={event => this.setState(byPropKey('comment', event.target.value))}></textarea>
                        <button className='report-send-button' onClick={() => this.sendReport()} disabled={buttonSendDisabled}>Send</button>
                    </div>
                }
            </div>
        )
    }
}

const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
})

export default withRouter(ProfilePage)
