import React, { Component } from 'react'
import './Chat.css'
import { withRouter } from 'react-router-dom'
import ItemsApi from '../../api/items-api'
import UsersApi from '../../api/users-api';
import * as routes from '../../constants/routes'

const itemsApi = new ItemsApi()
const usersApi = new UsersApi()

//idea: somehow attach the database listener on the chat and update
//      chat whenever new message received (no idea how to do it 
//      via RESTf)
class ChatPage extends Component {
    constructor(props) {
        super(props)
        
        this.state = {item: null, chat: null, currentUser: null, otherUser: null, message: ''}
    }

    /**
     * Gets chat object and item object related to the chat
     * and updates local state
     */
    componentDidMount = async () => {
        const chatToken = this.props.match.params.token
        const chat = await usersApi.getChat(chatToken)
        const item = await itemsApi.getItem(chat.item)
        this.setState({item: item, chat: chat})
    }

    /**
     * Gets user objects for participants of current chat
     *
     */
    getUsers = async () => {
        const currentUser = await usersApi.getUser(this.props.authUser.uid)
        let tempChat = this.state.chat
        tempChat.participants.splice(tempChat.participants.indexOf(this.props.authUser.uid), 1)
        const otherUser = await usersApi.getUser(tempChat.participants[0])
        this.setState({currentUser: currentUser, otherUser: otherUser})
    }

    /**
     * Sends a message and updates the chat
     *
     */
    sendMessage = async () => {
        const message = {
            uidFrom: this.props.authUser.uid,
            message: this.state.message,
            chatToken: this.state.chat.chatToken
        }
        await usersApi.sendMessage(message)
        const updatedChat = await usersApi.getChat(this.state.chat.chatToken)
        this.setState({chat: updatedChat, message: ''})
    }

    render() {
        if(this.state.currentUser === null && this.props.authUser && this.state.chat !== null) this.getUsers()

        const messages = []
        if(this.state.chat !== null && this.state.currentUser !== null && this.state.otherUser !== null) {
            for(const messageToken in this.state.chat.messages) {
                const message = this.state.chat.messages[messageToken]
                messages.push(
                    <div className='msg' style={{background: message.uid === this.props.authUser.uid ? '#34495e' : '#2980b9', color: 'white'}} key={messages.length} >
                        <span><b>{message.uid === this.props.authUser.uid ? this.state.currentUser.username : this.state.otherUser.username} </b></span>
                        <span><i>({formatTimestamp(message.timestamp)})</i>:</span>
                        <div style={{whiteSpace: 'pre-wrap'}}>{message.message}</div>
                    </div>
                )
            }
        }

        return(
            <div className='chat-page'>
                <h3>Chat</h3>
                <br/>
                { this.state.chat === null || this.state.item === null ? null : 
                <div>
                    <span>Item: </span><span style={{cursor: 'pointer'}} onClick={() => this.props.history.push(`${routes.ITEM}/${this.state.chat.item}`)}><b>{this.state.item.title}</b></span>
                    <br/> <br/>
                    <div style={{overflowY: 'scroll', height: '300px'}}>
                        {messages}
                    </div>
                    <textarea className='new-message-text-area' placeholder='Type a message' value={this.state.message} onChange={event => this.setState(byPropKey('message', event.target.value))}/>
                    <br/>
                    <button className='send-message-button' onClick={() => this.sendMessage()}>Send</button>
                </div>
                }
            </div>
        )
    }
}

const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
})

function formatTimestamp(timestamp) {
    const date = new Date(timestamp)
    return `${('0' + (date.getDate() + 1)).slice(-2)}/${('0' + (date.getMonth() + 1)).slice(-2)}/${date.getFullYear()} - ${('0' + (date.getHours())).slice(-2)}:${('0' + (date.getMinutes())).slice(-2)}`
}

export default withRouter(ChatPage)