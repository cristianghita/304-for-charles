import React, { Component } from 'react';
import './App.css';
import { 
  BrowserRouter as Router,
  Route, 
} from 'react-router-dom';

import Navigation from '../navigation/Navigation'
import SignUpPage from '../sign-up/SignUp'
import SignInPage from '../sign-in/SignIn'
import HomePage from '../home/Home'
import AccountPage from '../account/Account'
import AddItemPage from '../add-item/Add-item'
import * as routes from '../../constants/routes'
import { auth } from '../../firebase/firebase'
import ItemPage from '../item/Item'
import ChatPage from '../chat/Chat'
import ProfilePage from '../profile/Profile'
import AdminPage from '../admin/Admin'

class App extends Component {
  constructor(props) {
    super(props)
    
    this.state = {
      authUser: null,
      searchItem: '',
      currentSearchItem: ''
    }
  }

  /**
   * Adds a listener for authenticated user. This user is passed to every component as a prop
   *
   * @memberof App
   */
  componentDidMount() {
    auth.onAuthStateChanged(authUser=> {
      authUser
        ? this.setState({ authUser })
        : this.setState({ authUser: null })
    })
  }

  /**
   * Updates the search term for homepage to pick up
   *
   * @param {*} event submit event
   */
  onSearchSubmit = async event => {
    event.preventDefault()
    this.setState(byPropKey('searchItem', this.state.currentSearchItem))
    //this.setState(byPropKey('currentSearchItem', ''))
  }
  
  render() {
    return (
        <Router>
          <div>
            <Navigation authUser={this.state.authUser} history={this.props.history}/>
            <form onSubmit={(event) => this.onSearchSubmit(event)} className='search-form'>
              <input type='text' placeholder='search' value={this.state.currentSearchItem} 
                onChange={(event) => this.setState(byPropKey('currentSearchItem', event.target.value))}/>
              <button type='submit'>Submit</button>
            </form>
            
            <Route
              exact path={routes.SIGN_UP}
              component={SignUpPage}
            />
            
            <Route
              exact path={routes.SIGN_IN}
              component={SignInPage}
            />
            
            <Route
              exact path={routes.HOME}
              render={(props) => <HomePage {...props} authUser={this.state.authUser} searchItem={this.state.searchItem}/>}
            />

            <Route
              path={routes.ADD_ITEM}
              render={(props) => <AddItemPage {...props} authUser={this.state.authUser}/>}
            />
            
            <Route
              path={routes.ACCOUNT}
              render={(props) => <AccountPage {...props} authUser={this.state.authUser}/>}
            />

            <Route
              path={`${routes.ITEM}/:token`}
              render={(props) => <ItemPage {...props} authUser={this.state.authUser} />}
            />

            <Route
              path={`${routes.CHAT}/:token`}
              render={(props) => <ChatPage {...props} authUser={this.state.authUser} />}
            />

            <Route
              path={`${routes.PROFILE}/:uid`}
              render={(props) => <ProfilePage {...props} authUser={this.state.authUser} />}
            />

            <Route
              path={routes.ADMIN}
              render={(props) => <AdminPage {...props} authUser={this.state.authUser} />}
            />

          </div>
        </Router>
    )
  }
}

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value,
})

export default App;
