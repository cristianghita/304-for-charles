import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import './Account.css'
import ItemsApi from '../../api/items-api'
import UsersApi from '../../api/users-api';
import * as routes from '../../constants/routes'

const itemsApi = new ItemsApi()
const usersApi = new UsersApi()

class AccountPage extends Component {
    constructor(props) {
        super(props)

        this.state = {hideUserItems: true, hideSavedItems: true, hideChats: true}
    }

    render() {
        return(
            <div className='account-page'>
            {!this.props.authUser ? <span style={{textAlign: 'center', display: 'block', margin: '0px auto'}}>Sign In to see this page.</span> : 
                <div className='account-page'>
                <h2>Your account</h2>
                <button onClick={() => this.setState(byPropKey('hideUserItems', !this.state.hideUserItems))} className='show-hide-button'>
                    {this.state.hideUserItems ? 'Show your items' : 'Hide your items'}
                </button>
                <button onClick={() => this.setState(byPropKey('hideSavedItems', !this.state.hideSavedItems))} className='show-hide-button'>
                    {this.state.hideSavedItems ? 'Show saved items' : 'Hide saved'}
                </button>
                <button onClick={() => this.setState(byPropKey('hideChats', !this.state.hideChats))} className='show-hide-button'>
                    {this.state.hideChats ? 'Show chats' : 'Hide chats'}
                </button>
                {
                    this.state.hideUserItems ?
                    null :
                    <div>
                        <UserItems authUser={this.props.authUser} history={this.props.history}/>
                        <div className='separator'></div>
                    </div>
                }
                {
                    this.state.hideSavedItems ?
                    null :
                    <div>
                        <SavedItems authUser={this.props.authUser} history={this.props.history}/>
                        <div className='separator'></div>
                    </div>
                }
                {
                    this.state.hideChats ?
                    null :
                    <div>
                        <Chats authUser={this.props.authUser} history={this.props.history}/>
                        <div className='separator'></div>
                    </div>
                }
                </div>
            }
            </div>
        )
    }
}

class UserItems extends Component {
    constructor(props) {
        super(props)
        this.state = {items: [], itemsLoaded: false}
    }

    /**
     * Gets user's items for sale and updates local state
     *
     */
    updateUserItems = async () => {
        const items = await itemsApi.getUserItems(this.props.authUser.uid)
        this.setState(byPropKey('items', items))
    }

    /**
     * Updates publish status of an item 
     *
     * @param {*} token token of the target item
     */
    togglePublish = async token => {
        const published = !(this.state.items.find(item => item.token === token).published)
        await itemsApi.setPublished(token, published)

        const myItems = this.state.items
        const itemIndex = myItems.findIndex(item => item.token === token)
        myItems[itemIndex].published = published
        this.setState(byPropKey('items', myItems))
    }

    render() {
        if(!this.state.itemsLoaded && this.state.items.length === 0 && this.props.authUser) {
            this.setState(byPropKey('itemsLoaded', true))
            this.updateUserItems()
        }

        return(
            <div>
                <h3>{this.props.differentUser ? 'Items for sale' : 'Your items'}</h3>
                {this.state.items.map(item => 
                    this.props.differentUser 
                        ? (!item.published ? null
                            : 
                            <div className='user-item' id={`item-${item.token}`} key={item.token}>
                                <img className='user-item-image' src={item.images[0]} alt=''/>
                                <div key={item.token} className='item-info'>
                                    <span className='item-title' onClick={() => this.props.history.push(`${routes.ITEM}/${item.token}`)}>{item.title}</span>
                                    <br/>
                                    <span>£{item.price.toFixed(2)}</span>
                                    {this.props.differentUser ? null : <button className='publish-button' onClick={() => this.togglePublish(item.token)}>{item.published ? 'Unpublish' : 'Publish'}</button>}
                                </div>
                            </div>)
                        : 
                        <div className='user-item' id={`item-${item.token}`} key={item.token}>
                        <img className='user-item-image' src={item.images[0]} alt=''/>
                        <div key={item.token} className='item-info'>
                            <span className='item-title' onClick={() => this.props.history.push(`${routes.ITEM}/${item.token}`)}>{item.title}</span>
                            <br/>
                            <span>£{item.price.toFixed(2)}</span>
                            {this.props.differentUser ? null : <button className='publish-button' onClick={() => this.togglePublish(item.token)}>{item.published ? 'Unpublish' : 'Publish'}</button>}
                        </div>
                    </div>
                    
                )}
            </div>
        )
    }
}

class SavedItems extends Component {
    constructor(props) {
        super(props)
        this.state = {items: [], itemsLoaded: false}
    }

    /**
     * Gets the saved (starred) items of an user and updates the local state
     *
     */
    updateSavedItems = async () => {
        const itemTokens = await usersApi.getUserSavedItems(this.props.authUser.uid)
        const items = []
        for (const i in itemTokens) {
            items.push(await itemsApi.getItem(itemTokens[i]))
        }
        this.setState(byPropKey('items', items))
    }

    render() {
        if(!this.state.itemsLoaded && this.state.items.length === 0 && this.props.authUser) {
            this.setState(byPropKey('itemsLoaded', true))
            this.updateSavedItems()
        }

        return (
            <div>
                <h3>Saved items</h3>
                {this.state.items.map(item => 
                    <div className='user-item' id={`item-${item.token}`} key={item.token}>
                        <img className='user-item-image' src={item.images[0]} alt=''/>
                        <div key={item.token} className='item-info'>
                            <span className='item-title' onClick={() => this.props.history.push(`${routes.ITEM}/${item.token}`)}>{item.title}</span>
                            <br/>
                            <span>£{item.price.toFixed(2)}</span>
                        </div>
                    </div>
                )}
            </div>
        )
    }
}

class Chats extends Component {
    constructor(props) {
        super(props)
        this.state = {chats: [], chatsJsx: [], chatsLoaded: false}
    }

    /**
     * Loads user's chats and updates local state 
     *
     */
    updateChats = async () => {
        this.setState(byPropKey('chats', await usersApi.getUserChats(this.props.authUser.uid)))
    }

    /**
     * Creates the JSX object for each chat
     *
     */
    updateChatsJsx = async () => {
        const chats = []
        
        for (const i in this.state.chats) {
            let chat = this.state.chats[i]
            chat.participants.splice(chat.participants.indexOf(this.props.authUser.uid), 1) //remove current user from participants
            const participantUsername = (await usersApi.getUser(chat.participants[0])).username

            //order messages chronologically
            let messagesArray = []
            for (const msgToken in chat.messages) {
                const myMessage = chat.messages[msgToken]
                myMessage['token'] = msgToken
                messagesArray.push(myMessage)
            }

            messagesArray = messagesArray.sort((a, b) => (a.timestamp > b.timestamp) ? 1 : ((b.timestamp < a.timestamp) ? -1 : 0))
            const lastMessage = messagesArray[messagesArray.length - 1]

            const item = await itemsApi.getItem(chat.item)

            chats.push(
                <div key={chat.chatToken} className='chat'>
                    <span>User: </span><button className='user-button' onClick={() => this.props.history.push(`${routes.PROFILE}/${chat.participants[0]}`)}>{participantUsername}</button>
                    <br/>
                    <span>Item: </span><span style={{cursor: 'pointer'}} onClick={() => this.props.history.push(`${routes.ITEM}/${chat.item}`)}><b>{item.title}</b></span>
                    <br/> <br/>
                    <span><b>Last message</b> ({lastMessage.uid === this.props.authUser.uid ? 'you' : participantUsername})</span>
                    <br />
                    <textarea className='last-msg-tarea' readOnly={true} value={lastMessage.message}></textarea>
                    <br/>
                    <button className='show-hide-button'
                    onClick={() => this.props.history.push(`${routes.CHAT}/${chat.chatToken}`)}>Open chat</button>
                </div>
            )
        }

        this.setState(byPropKey('chatsJsx', chats))
    }

    render() {
        if(!this.state.chatsLoaded && this.state.chats.length === 0 && this.props.authUser) {
            this.setState(byPropKey('chatsLoaded', true))
            this.updateChats()
        }

        if(this.state.chats.length !== this.state.chatsJsx.length) this.updateChatsJsx()

        return(
            <div>
                <h3>Your chats</h3>
                {this.state.chatsJsx}
            </div>
        )
    }
}

const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
})
    
export default withRouter(AccountPage)
export {UserItems}