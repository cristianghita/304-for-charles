import React, { Component } from 'react'
import { 
    Link,
    withRouter,
} from 'react-router-dom'
import * as routes from '../../constants/routes'
import * as auth from '../../firebase/auth'
import UsersApi from '../../api/users-api'
import './SignUp.css'

const usersApi = new UsersApi()

const INITIAL_STATE = {
    username: '',
    email: '',
    passwordOne: '',
    passwordTwo: '',
    error: null
}

const SignUpPage = ({ history }) =>
    <div className='sign-in-page'>
        <h2>SignUp</h2>
        <SignUpForm history={history}/>
    </div> 

const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
})
    
class SignUpForm extends Component {
    constructor(props) {
        super(props)
        
        this.state = { ...INITIAL_STATE }
    }
    
    /**
     * Tries to sign-up a user and logs in and redirects to homepage if successful
     *
     * @param {*} event submit event
     */
    onSubmit = async (event) => {
        event.preventDefault()
        const {
            username,
            email,
            passwordOne,
        } = this.state
        
        console.log('email: ' + email + ' password: ' + passwordOne + ' username: ' + username)

        const isUsernameAvailable = await usersApi.isUsernameAvailable(username)
        if(isUsernameAvailable) {
            try {
                const authUser = await auth.registerWithEmail(email, passwordOne)
                const user = {username: username, email: email, uid: authUser.user.uid}
                await usersApi.postNewUser(user)
                this.setState({ ...INITIAL_STATE})
                await delay(1500) //wait for user to get posted
                this.props.history.push(routes.HOME)
            } catch(err) {
                this.setState(byPropKey('error', err))
            }
        } else {
            this.setState(byPropKey('error'), Error(`username ${username} is already in use`))
        }
    }

    render() {
        const {
            username,
            email,
            passwordOne,
            passwordTwo,
            error
        } = this.state
        
        const isInvalid = 
            passwordOne !== passwordTwo ||
            passwordOne === '' ||
            email === '' ||
            username === ''

        return (
            <form onSubmit={this.onSubmit}>
                <input
                    value={username} className='sign-in-input'
                    onChange={event => this.setState(byPropKey('username', event.target.value))}
                    type="text"
                    placeholder="Username"
                />
                <input
                    value={email} className='sign-in-input'
                    onChange={event => this.setState(byPropKey('email', event.target.value))}
                    type="text"
                    placeholder="Email Address"
                />
                <input
                    value={passwordOne} className='sign-in-input'
                    onChange={event => this.setState(byPropKey('passwordOne', event.target.value))}
                    type="password"
                    placeholder="Password"
                />
                <input
                    value={passwordTwo} className='sign-in-input'
                    onChange={event => this.setState(byPropKey('passwordTwo', event.target.value))}
                    type="password"
                    placeholder="Confirm Password"
                />
                <button disabled={isInvalid} type="submit" className='sign-in-button'>
                    Sign Up
                </button>

                { error && <p id='p-error-message'>{error.message}</p> }
            </form>
        )
    }
}

const SignUpLink = () => 
    <p style={{marginTop: '10px'}}>
        Don't have an account?
        {' '}
        <Link to={routes.SIGN_UP} id='sign-up-link'>Sign Up</Link>
    </p>

function delay(timeout) {
    return new Promise((resolve) => {
      setTimeout(resolve, timeout);
    });
  }

//export default SignUpPage
export default withRouter(SignUpPage)

export {
    SignUpForm,
    SignUpLink,
}