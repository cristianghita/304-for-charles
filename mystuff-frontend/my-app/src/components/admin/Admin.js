import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import UsersApi from '../../api/users-api'
import './Admin.css'
import * as routes from '../../constants/routes'

const usersApi = new UsersApi()

class AdminPage extends Component {
    constructor(props) {
        super(props)

        this.state = {showReportedUsers: true, adminVerified: null}
    }

    /**
     * Gets user object and verifies if it had admin privileges and updates local state
     *
     */
    verifyAdmin = async () => {
        const isAdmin = (await usersApi.getUser(this.props.authUser.uid)).admin
        if (isAdmin) { this.setState({adminVerified: true}) } else { this.setState({adminVerified: false}) }
    }

    render() {
        if(this.props.authUser && this.state.adminVerified === null) this.verifyAdmin()

        return(
            <div className='admin-page'>
                {!this.state.adminVerified ? <span>Admin privileges not recognized</span> :
                    <div> 
                        <h2>Admin Page</h2>
                        <button onClick={() => this.setState(byPropKey('showReportedUsers', !this.state.showReportedUsers))}>{this.state.showReportedUsers ? 'Hide' : 'Show'} reported users</button>
                        {!this.state.showReportedUsers ? null : 
                            <ReportedUsers authUser={this.props.authUser} history={this.props.history}/>
                        }
                    </div>
                }
            </div>
        )
    }
}

class ReportedUsers extends Component {
    constructor(props) {
        super(props)

        this.state = {reportedUsers: null}
    }

    /**
     * Loads reported users after the component mounted and updates local state
     *
     */
    componentDidMount = async () => {
        let reportedUsers = await usersApi.getReportedUsers()
        for (const i in reportedUsers) {
            reportedUsers[i].usernameReported = (await usersApi.getUser(reportedUsers[i].uidReported)).username
            reportedUsers[i].usernameReporter = (await usersApi.getUser(reportedUsers[i].uidReporter)).username
        }
        this.setState({reportedUsers: reportedUsers})
    }

    render() {
        return(
            <div className='reported-users-block'>
                <h3>Reported Users</h3>
                {!this.state.reportedUsers ? null : 
                    this.state.reportedUsers.map((report, index) => 
                        <div className='reported-user-section' key={index}>
                            <div className='report-attribute'><span><b>User: </b></span>
                                <span onClick={() => this.props.history.push(`${routes.PROFILE}/${report.uidReported}`)} className='seller-button'>{report.usernameReported}</span></div>
                            <div className='report-attribute'><span><b>Reported by: </b></span>
                                <span onClick={() => this.props.history.push(`${routes.PROFILE}/${report.uidReporter}`)} className='seller-button'>{report.usernameReporter}</span></div>
                            <div className='report-attribute'><span><b>Timestamp: </b></span><span>{formatTimestamp(report.timestamp)}</span></div>
                            <textarea className='report-comment' value={report.comment} readOnly></textarea>
                        </div>
                    )
                }
            </div>
        )
    }
}

const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
})

function formatTimestamp(timestamp) {
    const date = new Date(timestamp)
    return `${('0' + (date.getDate() + 1)).slice(-2)}/${('0' + (date.getMonth() + 1)).slice(-2)}/${date.getFullYear()} - ${('0' + (date.getHours())).slice(-2)}:${('0' + (date.getMinutes())).slice(-2)}`
}

export default withRouter(AdminPage)
