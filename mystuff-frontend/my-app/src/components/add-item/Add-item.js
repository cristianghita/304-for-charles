import React, { Component } from 'react'
import * as routes from '../../constants/routes'
import ItemsApi from '../../api/items-api'
import { withRouter } from 'react-router-dom'
import './Add-item.css'

const itemsApi = new ItemsApi()

const INITIAL_STATE = {
    category: '',
    description: '',
    features: '',
    condition: '',
    title: '',
    price: 0.00,
    city: '',
    images: []
}

class AddItemPage extends Component {
    render() {
        return (
            <div className='add-item-page'>
                {!this.props.authUser ? <span>Sign In in order to add items</span> : 
                    <div>
                        <h2>Add Item</h2>
                        <AddItemForm history={this.props.history} authUser={this.props.authUser}/>  
                    </div>
                }
            </div>
        )
    }
}

class AddItemForm extends Component {
    constructor(props) {
        super(props)

        this.state = {...INITIAL_STATE, categories: [], conditions: []}
    }

    async componentDidMount() {
        this.setState(byPropKey('categories', await itemsApi.getAllCategories()))
        this.setState(byPropKey('conditions', await itemsApi.getAllConditions()))
    }

    /**
     * Converts uploaded images to base64 and stores them in the local state
     *
     * @param {*} selectorFiles images in Blob/File format
     * @memberof AddItemForm
     */
    async imagesChange(selectorFiles) {
        const base64 = []
        for(const i in selectorFiles) {
            const file = selectorFiles[i]
            if(typeof file === 'object') {
                let reader = new FileReader()
                reader.readAsDataURL(file)
                reader.onload = () => base64.push(reader.result)
            }
        }

        while(base64.length !== selectorFiles.length) await sleep(50)

        //remove base64 string header (e.g: "data:image/jpeg;base64,")
        // for (let i = 0; i < base64.length; i++) 
        //     base64[i] = base64[i].replace(base64[i].substring(0, base64[i].indexOf('base64') + 7), '')
        
        this.setState(byPropKey('images', base64))
    }

    /**
     * Adds the item to the database and forwards the user to the item page
     *
     * @param {*} event submit event
     */
    onSubmit = async event => {
        event.preventDefault()

        const item = {
            category: this.state.category,
            title: this.state.title,
            description: this.state.description,
            condition: this.state.condition,
            price: this.state.price,
            city: this.state.city,
            images: this.state.images,
            features: this.state.features.split(','),
            uid: this.props.authUser.uid
        }

        const dbitem = await itemsApi.postNewitem(item)
        this.setState({...INITIAL_STATE})
        this.props.history.push(`${routes.ITEM}/${dbitem.token}`)
    }

    render() {
        const isInvalid = 
            this.state.category === '' ||
            this.state.title === '' ||
            this.state.description === '' ||
            this.state.condition === '' ||
            this.state.price === 0 || 
            this.state.city === '' ||
            this.state.images.length === 0 ||
            this.state.features === ''

        return (
            <form onSubmit={this.onSubmit} className='add-item-form'>
                <div className='item-attribute-section'>
                    <label>Category</label> <br />
                    <select
                        value={this.state.category}
                        onChange={event => this.setState(byPropKey('category', event.target.value))}
                        id='category-dropdown' className='dropdown'>
                        <option value=''>Select One..</option>
                        {this.state.categories.map(category => (
                            <option
                                key={category}
                                value={category}
                                id={`category-option-${category}`}>
                                {category}
                            </option>
                        ))}
                    </select> <br />
                </div>
                
                <div className='item-attribute-section'>
                    <label>Condition</label> <br />
                    <select 
                        value={this.state.condition}
                        onChange={event => this.setState(byPropKey('condition', event.target.value))}
                        id='condition-dropdown' className='dropdown'>
                        <option value=''>Select One..</option>
                        {this.state.conditions.map(condition => (
                            <option
                                key={condition}
                                value={condition}
                                id={`condition-option-${condition}`}>
                                {condition}
                            </option>
                        ))}
                    </select> <br/>
                </div>

                <div className='item-attribute-section'>
                    <label>Title</label> <br />
                    <input type='text' value={this.state.title} onChange={event => this.setState(byPropKey('title', event.target.value))}
                        id='input-item-title' className='input-text'
                    /> <br />
                </div>
                
                <div className='item-attribute-section'>
                    <label>Description</label> <br />
                    <input type='text' value={this.state.description} onChange={event => this.setState(byPropKey('description', event.target.value))}
                        id='input-item-description' className='input-text'
                    /> <br />
                </div>

                <div className='item-attribute-section'>
                    <label>Price</label> <br />
                    <input type='number' step='0.01' value={this.state.price} onChange={event => this.setState(byPropKey('price', parseFloat(event.target.value)))}
                        id='input-item-price' className='input-number'
                    /> <br />
                </div>

                <div className='item-attribute-section'>
                    <label>City</label> <br />
                    <input type='text' value={this.state.city} onChange={event => this.setState(byPropKey('city', event.target.value))}
                        id='input-item-city' className='input-text'
                    /> <br />
                </div>

                <div className='item-attribute-section'>
                    <label>Images</label> <br />
                    <input id='file-upload' type='file' name='img' multiple onChange={event => this.imagesChange(event.target.files)}/> <br/>
                </div>

                <div className='item-attribute-section'>
                    <label>Features (separated by comma)</label> <br />
                    <input type='text' value={this.state.features} onChange={event => this.setState(byPropKey('features', event.target.value))}
                        id='input-item-features' className='input-text'
                    /> <br />
                </div>

                <button type='submit' disabled={isInvalid} id='submit-new-item-button'>Submit</button>
            </form>
        )
    }
}

const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
})

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export default withRouter(AddItemPage)
