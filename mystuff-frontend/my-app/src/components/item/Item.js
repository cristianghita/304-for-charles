import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import ItemsApi from '../../api/items-api'
import UsersApi from '../../api/users-api'
import './Item.css'
import * as routes from '../../constants/routes'

// right arrow: https://i.imgur.com/K1Tja1k.png
// left arrow: https://i.imgur.com/TrQLaxb.png

const itemsApi = new ItemsApi()
const usersApi = new UsersApi()

class ItemPage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            item: null, 
            username: '', 
            currentImgIndex: 0, 
            messageAreaVisible: false,
            message: '',
        }
    }

    /**
     * Loads current item object and the seller's user object
     * and updates local state
     * @memberof ItemPage
     */
    async componentDidMount() {
        const item = await itemsApi.getItem(this.props.match.params.token)
        this.setState(byPropKey('item', item))
        const user = await usersApi.getUser(item.uid)
        this.setState(byPropKey('username', user.username))
    }

    /**
     * Changes image source of the current displaying image
     *
     * @param {*} forward boolean - next = true, previous = false
     */
    changeImg = async forward => {
        if(forward && this.state.currentImgIndex + 1 < this.state.item.images.length) {
            this.setState(byPropKey('currentImgIndex', this.state.currentImgIndex + 1))
        }

        if(!forward && this.state.currentImgIndex > 0) {
            this.setState(byPropKey('currentImgIndex', this.state.currentImgIndex - 1))
        }
    }

    /**
     * Sends a message to item's owner
     * and redirects user to chat page
     */
    sendMessage = async () => {
        if(this.state.message === '') return

        const message = {
            uidFrom: this.props.authUser.uid,
            uidTo: this.state.item.uid,
            message: this.state.message,
            relatedItemToken: this.state.item.token
        }
        
        const msg = await usersApi.sendMessage(message)
        this.props.history.push(`${routes.CHAT}/${msg.chatToken}`)
    }

    render() {
        const features = []
        if(this.state.item !== null)
        {
            for (const i in this.state.item.features) {
                const feature = this.state.item.features[i]
                features.push(
                    <span key={i}> {feature}{i < this.state.item.features.length - 1 ? ',' : ''}</span>
                )
            }
        }

        return (
            <div id='item-page' className='centered'>
                {this.state.item === null ? null :
                    <div>
                        <div className='image-container'>
                            <img src={this.state.item.images[this.state.currentImgIndex]} id='item-image' alt=''/>
                            <input alt=''
                                type='image' id='previous-image-button'
                                src='https://i.imgur.com/TrQLaxb.png'
                                onClick={() => this.changeImg(false)}
                            />
                            <input alt=''
                                type='image' id='next-image-button'
                                src='https://i.imgur.com/K1Tja1k.png'
                                onClick={() => this.changeImg(true)}
                            />
                            <span className='img-counter'>{this.state.currentImgIndex + 1}/{this.state.item.images.length}</span>
                        </div>
                        <div id='item-details'>
                            <span><b>{this.state.item.title}</b></span>
                            <br /> <br />
                            {this.props.authUser && this.props.authUser.uid !== this.state.item.uid
                            ? <div>
                                    <button className='button-item' 
                                        onClick={() => this.setState(byPropKey('messageAreaVisible', !this.state.messageAreaVisible))}>Message seller</button>
                                    <br />
                              </div>
                            : null}
                            {this.state.messageAreaVisible
                            ? <div>
                                    <textarea className='message-text-area' placeholder='your message' value={this.state.message}
                                        onChange={event => this.setState(byPropKey('message', event.target.value))} autoFocus/> 
                                    <br/>
                                    <button className='button-item' style={{'margin': '0 0 0 4px'}}
                                        onClick={() => this.sendMessage()}
                                    >Send</button>
                            </div>
                            : null}
                            <span><b>Seller</b>: <button className='seller-button' onClick={() => this.props.history.push(`${routes.PROFILE}/${this.state.item.uid}`)}>{this.state.username}</button></span>
                            <br />
                            <span><b>Posted on</b>: {formatDate(this.state.item.timestamp)}</span>
                            <br />
                            <span><b>Price</b>: £{this.state.item.price.toFixed(2)}</span>
                            <br />
                            <span><b>Item condition</b>: {this.state.item.condition}</span>
                            <br />
                            <span><b>Location</b>: {this.state.item.city}</span>
                            <br />
                            <span><b>Features</b>: {features}</span>
                            <br />
                            <span><b>Description</b></span>
                            <br/>
                            <span>{this.state.item.description}</span>
                            <br />
                            

                        </div>
                    </div>
                }
            </div>
        )
    }
}

const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
})

const formatDate = timestmap => {
    const date = new Date(timestmap)
    return `${date.getDate() + 1}.${date.getMonth() + 1}.${date.getFullYear()}`
}

export default withRouter(ItemPage)
