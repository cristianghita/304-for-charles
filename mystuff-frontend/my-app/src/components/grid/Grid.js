import React, { Component } from 'react';

import './Grid.css';
import Card from '../card/Card';

class Grid extends Component {
    
    GridRow(cards, id) {
        if (cards === null || cards === undefined) {
            return null;
        }
        
        let fillEmptySpace;
        let colUnit = parseInt(this.props.colClass.substr(-1));
        let colClassType = this.props.colClass.substr(0, this.props.colClass.length - 1);
        
        if(this.props.rowLength * colUnit < 12) {
            let centerSpace = Math.floor((12 - (this.props.rowLength * colUnit)) / 2);
            let colClass_temp = colClassType + centerSpace;
            fillEmptySpace = (<div className={colClass_temp}>&nbsp;</div>);
        }
        else {
            fillEmptySpace = null;
        }

        for (const i in cards) {
            if(cards[i] === undefined) continue
            cards[i]['saved'] = this.props.savedItems ? this.props.savedItems.includes(cards[i].token) : undefined
        }

        return (
            <div className="row" key={id}>
                {fillEmptySpace}
                {cards.map((item, index) =>
                    <div className={this.props.colClass} key={index}>
                        {item === undefined ? null 
                        : 
                    <Card   category = {item.category}
                        city = {item.city}
                        condition = {item.condition}
                        description = {item.description}
                        price = {item.price}
                        timestamp = {item.timestamp}
                        title = {item.title}
                        token = {item.token}
                        uid = {item.uid}
                        onClick = {this.props.onClick}
                        images = {item.images}
                        features = {item.features}
                        onStarClick = {this.props.onStarClick}
                        authUser = {this.props.authUser}
                        alreadySaved = {item.saved}
                    />}
                        
                    </div>
                    )}
            </div>
        );
    }
    
    render() {
        if(this.props.items == null) {
            return null;
        }
        
        var allRows = [];
        var len = this.props.items.length;
        var totalRows = len / this.props.rowLength;
        var countRows = 0;
        
        while (countRows < totalRows) {
            let newRow = [];
            for (var i = 0; i < this.props.rowLength; i++){
                newRow.push(this.props.items[i + (countRows * this.props.rowLength)]);
            }
            countRows++;
            allRows.push(this.GridRow(newRow, countRows));
        }
        
        return (
            <div>
                {allRows}
            </div>
        );
    }
}

export default Grid;