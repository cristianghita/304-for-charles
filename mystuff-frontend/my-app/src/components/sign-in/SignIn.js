import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { SignUpLink } from '../sign-up/SignUp'
import * as auth from '../../firebase/auth'
import * as routes from '../../constants/routes'
import './SignIn.css'

const SignInPage = ({ history }) => 
    <div className='sign-in-page'>
        <h2>Sign In</h2>
        <SignInForm history={history} />
        <SignUpLink />
    </div>
    
const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
})
    
const INITIAL_STATE = {
    email: '',
    password: '',
    error: null,
    redirectToHome: false
}

class SignInForm extends Component {
    constructor(props) {
        super(props)
        
        this.state = { ...INITIAL_STATE}
    }
    
    /**
     * Tries signing in the user and redirects to homepage if succesful
     *
     * @param {*} event
     */
    onSubmit = async (event) => {
        event.preventDefault()
        
        try {
            await auth.signInWithEmail(this.state.email, this.state.password)
            this.setState({...INITIAL_STATE})
            this.props.history.push(routes.HOME)
        } catch(err) {
            this.setState(byPropKey('error', err))
        }
    }
    
    render() {
        const { email, password, error } = this.state
        
        const isInvalid = 
            password === '' ||
            email === ''

        return (
            <form onSubmit={this.onSubmit} className='sign-in-form'>
                <input
                    value={email} className='sign-in-input'
                    onChange={event => this.setState(byPropKey('email', event.target.value))}
                    type="text"
                    placeholder="Email Address"
                />
                <input
                    value={password} className='sign-in-input'
                    onChange={event => this.setState(byPropKey('password', event.target.value))}
                    type="password"
                    placeholder="Password"
                />
                <button disabled={isInvalid} type="submit" className='sign-in-button'>
                    Sign In
                </button>
        
                { error && <p>{error.message}</p> }
                
            </form>
        )
    }
}
    
export default withRouter(SignInPage)

export {
    SignInForm
}
