import React, { Component } from 'react'
import './Navigation.css'
import * as routes from '../../constants/routes'
import { withRouter } from 'react-router-dom'
import * as auth from '../../firebase/auth'
import UsersApi from '../../api/users-api'

const usersApi = new UsersApi()

class Navigation extends Component {
    constructor(props) {
        super(props)

        this.state = {user: null}
    }

    /**
     * Sign out the user and redirects to homepage
     *
     */
    signOut = async () => {
        auth.doSignOut()
        this.props.history.push(routes.HOME)
    }

    /**
     * Updates local user with current authenticated user
     *
     */
    updateUser = async () => {
        if(this.props.authUser === null) this.setState({user: null})
        else {
            const user = await usersApi.getUser(this.props.authUser.uid)
            this.setState({user: user})
        }
    }

    render() {
        if((!this.state.user && this.props.authUser) || (this.state.user && !this.props.authUser)) this.updateUser()
        const path = this.props.location.pathname
        return(
            <div className='header'>
                <button className='logo' onClick={() => this.props.history.push(routes.HOME)}>My Stuff</button>
                {this.props.authUser
                ? 
                    <div className='header-right'>
                        <button className={path === routes.HOME ? 'active' : 'inactive'} onClick={() => this.props.history.push(routes.HOME)}>Home</button>
                        {this.state.user && !this.state.user.admin ? null : <button className={path === routes.ADMIN ? 'active' : 'inactive'} onClick={() => this.props.history.push(routes.ADMIN)}>Admin</button>}
                        <button className={path === routes.ACCOUNT ? 'active' : 'inactive'} onClick={() => this.props.history.push(routes.ACCOUNT)}>Account</button>
                        <button className={path === routes.ADD_ITEM ? 'active' : 'inactive'} onClick={() => this.props.history.push(routes.ADD_ITEM)}>Add Item</button>
                        <button onClick={() => this.signOut()}>Sign Out</button>
                        {this.state.user === null ? null : <span>Logged in as {this.state.user.username}</span>}
                    </div>
                : 
                    <div className='header-right'>
                        <button className={path === routes.HOME ? 'active' : 'inactive'} onClick={() => this.props.history.push(routes.HOME)}>Home</button>    
                        <button className={path === routes.SIGN_IN ? 'active' : 'inactive'} onClick={() => this.props.history.push(routes.SIGN_IN)}>Sign In</button>
                    </div>}
            </div>
        )
    }
}

export default withRouter(Navigation)