import React, {Component} from 'react'
import './Card.css'
import './tooltip.css'
import FontAwesome from 'react-fontawesome'

class Card extends Component {
    
    constructor(props) {
        super(props);
        
        this.state = {
            cardStyle: {backgroundColor:this.props.backgroundColor},
            bookmark: false,
            username: '',
            triggeredInitialState: false
        };
        
        this.onClickHandler = this.onClickHandler.bind(this);
        this.onBookmarkClicked = this.onStarClick.bind(this);
    }
    /*
    category, city, condition, description, price, published, timestamp, title, token, uid
    features[], images[]
    */
    onClickHandler(event) {
        event.preventDefault()
        this.props.onClick(this.props.token)
    }
    
    onStarClick(event) {
        let currentValue = this.state.bookmark
        this.setState({bookmark:!currentValue})
        this.props.onStarClick(this.props.token, !currentValue)
    }

    render() {
        if(!this.state.triggeredInitialState && this.props.alreadySaved !== undefined) {
            this.setState({triggeredInitialState: true, bookmark: this.props.alreadySaved})
        }

        let bookmarkIcon
        let toolTipMessage

        if(this.state.bookmark) {
            bookmarkIcon = <FontAwesome name='fas fa-star' className="bookmark bookmarked" size="2x" />
            toolTipMessage = "unsave";
        }
        else {
            bookmarkIcon = <FontAwesome name='fas fa-star' className="bookmark" size="2x" />
            toolTipMessage = "save";
        }

        return (
            <div className="card">
                { this.props.authUser
                    ?
                    <div>
                        <span className='clickableAwesomeFont tooltip' onClick={() => this.onStarClick()}>
                        <span className="tooltiptext">{toolTipMessage}</span>
                        {bookmarkIcon}
                        </span>
                    </div>
                    : null
                }
                <img src={this.props.images[0]} alt={this.props.imgAlt} />
                <div className="container">
                    <button onClick={this.onClickHandler} className="linkButton"><h4><b>{this.props.title}</b></h4></button>
                    <label>£{this.props.price.toFixed(2)}</label>
                    <br />
                    <label>{this.props.city}</label>
                    <br />
                </div>
            </div>
            );
    }
}

export default Card;