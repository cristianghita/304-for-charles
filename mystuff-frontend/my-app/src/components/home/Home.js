import React, { Component } from 'react' 
import * as routes from '../../constants/routes'
import Grid from '../grid/Grid'
import ItemsApi from '../../api/items-api';
import { withRouter } from 'react-router-dom'
import UsersApi from '../../api/users-api';

const itemsApi = new ItemsApi()
const usersApi = new UsersApi()

class HomePage extends Component {
    constructor(props) {
        super(props)
        this.state = {searchItem: '', items: []}
    }

    /**
     * Searches an empty string retrieving all items
     *
     */
    componentDidMount = async () => {
        this.doSearchItem(this.state.searchItem)
    }

    /**
     * Searches items based on search string input 
     *
     * @param {*} searchItem
     */
    doSearchItem = async searchItem => {
        const items = await itemsApi.getItems(searchItem)
        this.setState(byPropKey('items', items))
    }

    render() {
        if(this.props.searchItem !== this.state.searchItem) {
            this.setState(byPropKey('searchItem', this.props.searchItem))
            this.doSearchItem(this.props.searchItem)
        }

        return (
            <div>
                <ItemsGrid items={this.state.items} history={this.props.history} authUser={this.props.authUser}/>
            </div>
        )
    }
}

class ItemsGrid extends Component {
    constructor(props) {
        super(props)
        this.state = {savedItems: undefined, savedItemsRetrieved: false}
    }

    /**
     * Saves/unsaves an item for current user
     *
     * @param {*} token item's token
     * @param {*} save true/false
     */
    itemSaveClick = async (token, save) => {
        usersApi.saveUnsaveItem(token, save, this.props.authUser.uid)
    }

    render() {
        const items = this.props.items === null || this.props.items === undefined ? [] : this.props.items
        if(this.props.authUser !== null && !this.state.savedItemsRetrieved) {
            usersApi.getUser(this.props.authUser.uid).then((res) => {
                const savedItems = []
                for (const token in res['saved-items']) { savedItems.push(token) }
                this.setState(byPropKey('savedItems', savedItems))
                this.setState(byPropKey('savedItemsRetrieved', true))
            })
        }

        return (
            <Grid 
                items={items} 
                colClass='col-m-3' 
                onClick={token => this.props.history.push(`${routes.ITEM}/${token}`)}
                onStarClick={(token, save) => this.itemSaveClick(token, save)}
                rowLength={4}
                authUser={this.props.authUser}
                savedItems={this.state.savedItems}/>
        )
    }
}

const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
})

export default withRouter(HomePage)