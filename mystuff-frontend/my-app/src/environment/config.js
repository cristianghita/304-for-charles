/**
 * Contains api routes
 *
 * @class config
 */
class config {
    static api_host = 'https://mystuff-bend-docker.herokuapp.com'//'http://localhost:8080'
    static api_path = '/api/v1.0'
    static host_path = `${config.api_host}${config.api_path}`

    static api_users = `${config.host_path}/users`
    static api_items = `${config.host_path}/items`

    static api_users_get_isUsernameAvailable = config.api_users + '/isusernameavailable'
    static api_users_post_newUser = config.api_users + '/new'
    static api_users_post_message = config.api_users + '/message'
    static api_users_post_report = config.api_users + '/report'
    static api_users_put_saveUnsaveItem = config.api_users + '/save-unsave-item'
    static api_users_put_setRating = config.api_users + '/user-rating'
    static api_users_get_user = `${config.api_users}/user`
    static api_users_get_savedItems = `${config.api_users}/saved-items`
    static api_users_get_userChats = `${config.api_users}/user-chats`
    static api_users_get_chat = `${config.api_users}/chat`
    static api_users_get_reported = `${config.api_users}/reported`

    static api_items_get_categories = `${config.api_items}/item-categories`
    static api_items_get_conditions = `${config.api_items}/item-conditions`
    static api_items_get_items = `${config.api_items}/items`
    static api_items_get_user_items = `${config.api_items}/user-items`
    static api_items_get_item = `${config.api_items}/item`
    static api_items_post_new = `${config.api_items}/new`
    static api_items_put_published = `${config.api_items}/item-published`
}

export default config