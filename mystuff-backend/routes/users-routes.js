'use strict'

const status = require('http-status-codes')
const userProcessor = require('../modules/processors/users-processor')

const api_v10 = '/api/v1.0'

const router = require('koa-router')({
	prefix: `${api_v10}/users`
})

router.post('/new', async ctx => {
	ctx.set('Allow', 'POST')
	try {
		if(ctx.get('error')) throw new Error(ctx.get('error'))
		const user = await userProcessor.createUser(await ctx.request)
		ctx.status = status.CREATED
		ctx.body = {status: 'success', message: {user: user}}
	} catch(err) {
		ctx.status = status.BAD_REQUEST
		ctx.body = {status: 'error', message: err.message}
	}
})

router.post('/message', async ctx => {
	ctx.set('Allow', 'POST')
	try {
		if(ctx.get('error')) throw Error(ctx.get('error'))
		const message = await userProcessor.sendMessage(await ctx.request)
		ctx.status = status.CREATED
		ctx.body = {status: 'success', message: {message: message}}
	} catch(err) {
		ctx.status = status.BAD_REQUEST
		ctx.body = {status: 'error', message: err.message}
	}
})

router.post('/report', async ctx => {
	ctx.set('Allow', 'POST')
	try {
		if(ctx.get('error')) throw Error(ctx.get('error'))
		const report = await userProcessor.reportUser(await ctx.request)
		ctx.status = status.CREATED
		ctx.body = {status: 'success', message: {report: report}}
	} catch(err) {
		ctx.status = status.BAD_REQUEST
		ctx.body = {status: 'error', message: err.message}
	}
})

router.put('/user-rating', async ctx => {
	ctx.set('Allow', 'PUT')
	try {
		if(ctx.get('error')) throw Error(ctx.get('error'))
		const rating = await userProcessor.setRating(await ctx.request)
		ctx.status = status.OK
		ctx.body = {status: 'success', message: {rating: rating}}
	} catch(err) {
		ctx.status = status.BAD_REQUEST
		ctx.body = {status: 'error', message: err.message}
	}
})

router.put('/save-unsave-item', async ctx => {
	ctx.set('Allow', 'PUT')
	try {
		if(ctx.get('error')) throw new Error(ctx.get('error'))
		const item = await userProcessor.saveUnsaveItem(await ctx.request)
		ctx.status = status.OK
		ctx.body = {status: 'success', message: {item: item}}
	} catch (err) {
		ctx.status = status.BAD_REQUEST
		ctx.body = {status: 'error', message: err.message}
	}
})

router.get('/isusernameavailable/:username', async ctx => {
	ctx.set('Allow', 'GET')
	try {
		if(ctx.get('error')) throw new Error(ctx.get('error'))
		if(ctx.params.username.toString() === 'undefined' || ctx.params.username === ' ') throw new Error('username not provided')
		const available = await userProcessor.isUsernameAvailable({body: {username: ctx.params.username}})
		ctx.status = status.OK
		ctx.body = {status: 'success', message: available}
	} catch(err) {
		ctx.status = status.BAD_REQUEST
		ctx.body = {status: 'error', message: err.message}
	}
})

router.get('/user/:uid', async ctx => {
	ctx.set('Allow', 'GET')
	try {
		if(ctx.get('error')) throw new Error(ctx.get('error'))
		const user = await userProcessor.getUser(ctx.params.uid)
		ctx.status = status.OK
		ctx.body = {status: 'success', message: {user: user}}
	} catch(err) {
		ctx.status = status.NOT_FOUND
		ctx.body = {status: 'error', message: err.message}
	}
})

router.get('/saved-items/:uid', async ctx => {
	ctx.set('Allow', 'GET')
	try {
		if(ctx.get('error')) throw new Error(ctx.get('error'))
		const items = await userProcessor.getUserSavedItems(ctx.params.uid)
		ctx.status = status.OK
		ctx.body = {status: 'success', message: {items: items}}
	} catch(err) {
		ctx.status = status.NOT_FOUND
		ctx.body = {status: 'error', message: err.message}
	}
})

router.get('/reported', async ctx => {
	ctx.set('Allow', 'GET')
	try {
		if(ctx.get('error')) throw new Error(ctx.get('error'))
		const reportedUsers = await userProcessor.getReportedUsers()
		ctx.status = status.OK
		ctx.body = {status: 'success', message: {reportedUsers: reportedUsers}}
	} catch(err) {
		ctx.status = status.NOT_FOUND
		ctx.body = {status: 'error', message: err.message}
	}
})

router.get('/user-chats/:uid', async ctx => {
	ctx.set('Allow', 'GET')
	try {
		if(ctx.get('error')) throw new Error(ctx.get('error'))
		const chats = await userProcessor.getUserChats(ctx.params.uid)
		ctx.status = status.OK
		ctx.body = {status: 'success', message: {chats: chats}}
	} catch(err) {
		ctx.status = status.NOT_FOUND
		ctx.body = {status: 'error', message: err.message}
	}
})

router.get('/chat/:chatToken', async ctx => {
	ctx.set('Allow', 'GET')
	try {
		if(ctx.get('error')) throw new Error(ctx.get('error'))
		const chat = await userProcessor.getChat(ctx.params.chatToken)
		ctx.status = status.OK
		ctx.body = {status: 'success', message: {chat: chat}}
	} catch(err) {
		ctx.status = status.NOT_FOUND
		ctx.body = {status: 'error', message: err.message}
	}
})

module.exports = router
