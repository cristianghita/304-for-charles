'use strict'

const status = require('http-status-codes')
const items_proc = require('../modules/processors/items-processor')

const api_v10 = '/api/v1.0'

const router = require('koa-router')({
	prefix: `${api_v10}/items`
})

router.post('/new', async ctx => {
	ctx.set('Allow', 'POST')
	try {
		if(ctx.get('error')) throw new Error(ctx.get('error'))
		const item = await items_proc.createForSale(await ctx.request)
		ctx.status = status.CREATED
		ctx.body = {status: 'success', message: {item: item}}
	} catch(err) {
		ctx.status = status.BAD_REQUEST
		ctx.body = {status: 'error', message: err.message}
	}
})

router.put('/item-published', async ctx => {
	ctx.set('Allow', 'PUT')
	try {
		if(ctx.get('error')) throw Error(ctx.get('error'))
		await items_proc.setPublished(await ctx.request)
		ctx.status = status.OK
		ctx.body = {status: 'success', message: 'updated succesfully'}
	} catch(err) {
		ctx.status = status.BAD_REQUEST
		ctx.body = {status: 'error', message: err.message}
	}
})

router.get('/item-conditions', async ctx => {
	ctx.set('Allow', 'GET')
	try {
		if(ctx.get('error')) throw new Error(ctx.get('error'))
		const conditions = await items_proc.getItemConditions()
		ctx.status = status.OK
		ctx.body = {status: 'success', message: {conditions: conditions}}
	} catch(err) {
		ctx.status = status.NOT_FOUND
		ctx.body = {status: 'error', message: err.message}
	}
})

router.get('/item-categories', async ctx => {
	ctx.set('Allow', 'GET')
	try {
		if(ctx.get('error')) throw new Error(ctx.get('error'))
		const categories = await items_proc.getItemCategories()
		ctx.status = status.OK
		ctx.body = {status: 'success', message: {categories: categories}}
	} catch(err) {
		ctx.status = status.NOT_FOUND
		ctx.body = {status: 'error', message: err.message}
	}
})

router.get('/items', async ctx => {
	ctx.set('Allow', 'GET')
	try {
		if(ctx.get('error')) throw new Error(ctx.get('error'))
		let keywords = ctx.query.keywords.split(',')
		if (keywords.length === 1 && keywords[0] === '') keywords = undefined
		const items = await items_proc.getItems(keywords)
		ctx.status = status.OK
		ctx.body = {status: 'success', message: {items: items}}
	} catch(err) {
		ctx.status = status.NOT_FOUND
		ctx.body = {status: 'error', message: err.message}
	}
})

router.get('/item/:token', async ctx => {
	ctx.set('Allow', 'GET')
	try {
		if(ctx.get('error')) throw new Error(ctx.get('error'))
		const item = await items_proc.getItem(ctx.params.token)
		ctx.status = status.OK
		ctx.body = {status: 'success', message: {item: item}}
	} catch(err) {
		ctx.status = status.NOT_FOUND
		ctx.body = {status: 'error', message: err.message}
	}
})

router.get('/user-items/:uid', async ctx => {
	ctx.set('Allow', 'GET')
	try {
		if(ctx.get('error')) throw new Error(ctx.get('error'))
		const items = await items_proc.getUseritems(ctx.params.uid)
		ctx.status = status.OK
		ctx.body = {status: 'success', message: {items: items}}
	} catch(err) {
		ctx.status = status.NOT_FOUND
		ctx.body = {status: 'error', message: err.message}
	}
})

module.exports = router
