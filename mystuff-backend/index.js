'use strict'

const Koa = require('koa')
const bodyParser = require('koa-bodyparser')
const cors = require('@koa/cors')

const app = new Koa()
app.use(cors())
app.use(bodyParser())

const defaultPort = 8080
const port = process.env.PORT || defaultPort // uses given port or default if not assigned one (hosting purposes)

// splitting api into 2 parts
// users routes
const usersRouter = require('./routes/users-routes')
app.use(usersRouter.routes())
app.use(usersRouter.allowedMethods())
// items routes
const forSaleRouter = require('./routes/items-routes')
app.use(forSaleRouter.routes())
app.use(forSaleRouter.allowedMethods())

const server = app.listen(port)
console.log(`app running on port [${port}]`)

module.exports = server
