'use strict'

const db = require('./firebase').db

module.exports.createUser = async user => {
	const available = (await this.isUsernameAvailable(user)).available
	if(!available) throw Error('username not available')
	await db.ref(`users/${user.uid}`).set(user)
	return user
}

/**
 * Checks wether the username is in use or not by other user
 *
 * @param {*} user user object containing username attribute
 * @returns {available: boolean} {available: boolean}
 */
module.exports.isUsernameAvailable = async user => {
	const usersRef = db.ref('users').orderByChild('username').equalTo(user.username)
	const snapshot = await usersRef.once('value')

	if (snapshot.exists()) return {available: false}
	else return {available: true}
}

/**
 * Gets the whole value of a user in database
 *
 * @param {*} uid user authentication id
 * @returns {*} user value + token
 */
module.exports.getUser = async uid => {
	const snapshot = await db.ref(`/users/${uid}`).once('value')
	if(snapshot.exists()) {
		const user = snapshot.val()
		user['uid'] = uid
		return user
	}

	throw Error('user not found')
}

/**
 * Retrieves saved (starred) items for each user
 *
 * @param {*} uid user authentication id
 * @returns {*} array of string tokens
 */
module.exports.getUserSavedItems = async uid => {
	const snapshot = await db.ref(`/users/${uid}/saved-items`).once('value')
	const savedItems = []

	if(snapshot.exists()) {
		for(const itemToken in snapshot.val()) {
			savedItems.push(itemToken)
		}
	}

	return savedItems
}

/**
 * Saves or unsaves an item for a specific user
 *
 * @param {*} token item's unique token
 * @param {*} save boolean? - true = saves, null/undefined = unsave
 * @param {*} uid user authentication id
 * @returns
 */
module.exports.saveUnsaveItem = async (token, save, uid) => {
	const userSnapshot = await db.ref(`/users/${uid}`).once('value')

	if(userSnapshot.exists()) {
		await db.ref(`/users/${uid}/saved-items/${token}`).set(save ? save : null)
		return {token: token, save: save, uid: uid}
	}

	throw Error('user not found')
}

/**
 * Sends message to existing chat or creates new chat and sends the message to it
 *
 * @param {string} uidFrom uid of the sending user
 * @param {string} message text message to be sent
 * @param {string} chatToken chat token or NULL if chat does not exist
 * @param {string} uidTo uid of the receiving user or NULL if chat exists
 * @param {string} relatedItemToken token of the inquired item or NULL if chat exists
 * @returns {object} {chatToken: str, messageToken: str, uid: str, message: str, timestamp: int}
 */
module.exports.sendMessage = async (uidFrom, message, chatToken, uidTo, relatedItemToken) => {
	const timestamp = (new Date).getTime()
	const messageToAdd = {message: message, timestamp: timestamp, uid: uidFrom}

	if(chatToken === null) {
		const newChatToken = await db.ref('chats').push().key
		// set participants
		const participants = {0: uidFrom, 1: uidTo}
		await db.ref(`chats/${newChatToken}/participants`).set(participants)

		// set related item
		await db.ref(`chats/${newChatToken}/item`).set(relatedItemToken)

		// add message
		const messageToken = await db.ref(`chats/${newChatToken}/messages`).push().key
		await db.ref(`chats/${newChatToken}/messages/${messageToken}`).set(messageToAdd)

		const objToReturn = {chatToken: newChatToken, messageToken: messageToken, uid: uidFrom, message: message, timestamp: timestamp}
		return objToReturn
	} else {
		const chatSnapshot = await db.ref(`chats/${chatToken}`).once('value')

		if(!chatSnapshot.exists()) throw Error('chat not found')

		// add message
		const messageToken = await db.ref(`chats/${chatToken}/messages`).push().key
		await db.ref(`chats/${chatToken}/messages/${messageToken}`).set(messageToAdd)

		const objToReturn = {chatToken: chatToken, messageToken: messageToken, uid: uidFrom, message: message, timestamp: timestamp}
		return objToReturn
	}
}

/**
 * Gets all chats this user is a participant of
 *
 * @param {*} uid user authentication id
 * @returns {*} array of chat object
 */
module.exports.getUserChats = async uid => {
	const chatsToReturn = []
	const chats = (await db.ref('chats').once('value')).val()
	for (const chatToken in chats) {
		if (chats[chatToken].participants.includes(uid)) {
			const myChat = chats[chatToken]
			myChat['chatToken'] = chatToken
			chatsToReturn.push(myChat)
		}
	}
	return chatsToReturn
}

/**
 * Gets specific chat based on token
 *
 * @param {*} chatToken chat's unique token
 * @returns {*} chat object
 */
module.exports.getChat = async chatToken => {
	const snapshot = await db.ref(`chats/${chatToken}`).once('value')
	if(!snapshot.exists()) throw Error ('chat not found')

	const chat = snapshot.val()
	chat['chatToken'] = chatToken
	return chat
}

/**
 * Sets rating of a user for another user
 *
 * @param {*} uidFrom user that is rating
 * @param {*} uidTo user that is rated
 * @param {*} rating rated amount - int = rating, null = delete rating
 * @returns {*} rating object
 */
module.exports.setRating = async (uidFrom, uidTo, rating) => {
	await db.ref(`users/${uidTo}/ratings/${uidFrom}`).set(rating)
	return {fromUid: uidFrom, toUid: uidTo, rating: rating}
}

/**
 * Saves a user report
 *
 * @param {*} report contains {uidReporter, uidReported, comment, timestamp}
 * @returns {*} incoming report object + token
 */
module.exports.reportUser = async report => {
	const reportToken = await db.ref('report').push().key
	await db.ref(`report/${reportToken}`).set(report)
	report['token'] = reportToken
	return report
}

/**
 * Get an array of report tokens
 *
 * @returns {*} array of report tokens
 */
module.exports.getReportedUsers = async () => {
	const reportsToReturn = []

	const snapshot = await db.ref('report').once('value')
	const reports = snapshot.val()

	for	(const token in reports) {
		const report = reports[token]
		report['token'] = token
		reportsToReturn.push(report)
	}

	return reportsToReturn
}
