'use strict'

const firebase = require('firebase')

const config = {
	apiKey: 'AIzaSyBigYAcNw7iN0PoT0DZzWqy0xzpPZzAnZ4',
	authDomain: 'mystuff-backend.firebaseapp.com',
	databaseURL: 'https://mystuff-backend.firebaseio.com',
	projectId: 'mystuff-backend',
	storageBucket: 'mystuff-backend.appspot.com',
	messagingSenderId: '575662612722'
}

if(!firebase.apps.length) {
	firebase.initializeApp(config)
}

module.exports.db = firebase.database()
