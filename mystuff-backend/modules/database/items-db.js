'use strict'

const db = require('./firebase').db

/**
 * Adds an item for sale
 *
 * @param {*} item item with all attributes
 * @returns item object + token
 */
module.exports.createForSale = async item => {
	const token = await db.ref('items').push().key
	await db.ref(`items/${token}`).set(item)
	item['token'] = token
	return item
}

/**
 * Gets the conditions in database for an item
 *
 * @returns [string]
 */
module.exports.getItemConditions = async () => {
	const snapshot = await db.ref('item-conditions').once('value')
	if (snapshot.exists()) {
		return snapshot.val()
	}
}

/**
 * Gets the categories in database for an item
 *
 * @returns [string]
 */
module.exports.getItemCategories = async () => {
	const snapshot = await db.ref('item-categories').once('value')
	if (snapshot.exists()) {
		return snapshot.val()
	}
}

/**
 * Gets items and filters them based on keywords if any passed
 *
 * @param {*} keywords [string] of keywords
 * @returns [item] array of item objects
 */
module.exports.getItems = async keywords => {
	const items = []
	const snapshot = await db.ref('items').once('value')

	if(!snapshot.exists()) throw Error('items not found')

	if (keywords === undefined || keywords.length === 0) {
		const dbitems = snapshot.val()
		for (const token in dbitems) {
			const item = dbitems[token]
			item['token'] = token
			if (item.published) items.push(item)
		}
	} else {
		const dbitems = snapshot.val()
		for (const token in dbitems) {
			const item = dbitems[token]
			item['token'] = token

			if(keywords.some(x => item['title'].split(' ').includes(x))) {
				if (item.published) items.push(item)
				continue
			}

			if(item['features'] !== undefined && keywords.some(x => item['features'].includes(x))) items.push(item)
		}
	}

	return items
}

/**
 * Gets an item based on token
 *
 * @param {*} token item's unique token
 * @returns item object
 */
module.exports.getItem = async token => {
	const snapshot = await db.ref(`items/${token}`).once('value')

	if (!snapshot.exists()) throw Error('item not found')

	const item = snapshot.val()
	item['token'] = snapshot.key
	return item
}

/**
 * Retrieves all items that a user added for sale
 *
 * @param {*} uid user authentication id
 * @returns [item] array of item objects
 */
module.exports.getUseritems = async uid => {
	const snapshot = await db.ref('items').orderByChild('uid').equalTo(uid).once('value')
	const itemsToReturn = []

	if (snapshot.exists()) {
		const items = snapshot.val()
		for (const token in items) {
			const myItem = items[token]
			myItem['token'] = token
			itemsToReturn.push(myItem)
		}
	}

	return itemsToReturn
}

/**
 * Sets an item to be published or unpublished (default is published)
 *
 * @param {*} token item's unique token
 * @param {*} published boolean deciding if published or not
 */
module.exports.setPublished = async (token, published) => {
	const snapshot = await db.ref(`items/${token}`).once('value')

	if(!snapshot.exists()) throw Error('item not found')

	await db.ref(`items/${token}/published`).set(published)
}
