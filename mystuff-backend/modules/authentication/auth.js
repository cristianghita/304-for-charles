'use strict'

const firebase = require('../../node_modules/firebase')

const config = {
	apiKey: 'AIzaSyBigYAcNw7iN0PoT0DZzWqy0xzpPZzAnZ4',
	authDomain: 'mystuff-backend.firebaseapp.com',
	databaseURL: 'https://mystuff-backend.firebaseio.com',
	projectId: 'mystuff-backend',
	storageBucket: 'mystuff-backend.appspot.com',
	messagingSenderId: '575662612722'
}

if(!firebase.apps.length) {
	firebase.initializeApp(config)
}

const auth = firebase.auth()

module.exports.isUserValid = async email => {
	let isValid = false

	await auth.fetchSignInMethodsForEmail(email)
		.then((signInMethods) => {
			if(signInMethods.indexOf(firebase.auth.EmailAuthProvider.EMAIL_PASSWORD_SIGN_IN_METHOD) !== -1) {
				isValid = true
			}
		})
		.catch((error) => {
			console.log(error)
			//some error ocurred
		})

	return {valid: isValid}
}
