'use strict'

const users_db = require('../database/users-db')

/**
 * Creates a database user
 *
 * @param {*} request contains {uid, username, email}
 * @returns user {uid, username, email, token}
 */
module.exports.createUser = async request => {
	const user = await this.extractUserData(request)
	const usertoReturn = await users_db.createUser(user)
	return usertoReturn
}

/**
 * Checks wether the username is in use or not by other user
 *
 * @param {*} request contains {username}
 * @returns {*} {availbale: bool}
 */
module.exports.isUsernameAvailable = async request => {
	const user = await this.extractUserData(request)
	const available = await users_db.isUsernameAvailable(user)
	return available
}

/**
 * Gets the whole value of a user in database
 *
 * @param {*} uid user authentication id
 * @returns {*} user value + token
 */
module.exports.getUser = async uid => {
	const user = await users_db.getUser(uid)
	return user
}

/**
 * Retrieves saved (starred) items for each user
 *
 * @param {*} uid user authentication id
 * @returns {*} array of string tokens
 */
module.exports.getUserSavedItems = async uid => {
	const items = await users_db.getUserSavedItems(uid)
	return items
}

/**
 * Gets all chats this user is a participant of
 *
 * @param {*} uid user authentication id
 * @returns {*} array of chat object
 */
module.exports.getUserChats = async uid => {
	const chat = await users_db.getUserChats(uid)
	return chat
}

/**
 * Gets specific chat based on token
 *
 * @param {*} chatToken chat's unique token
 * @returns {*} chat object
 */
module.exports.getChat = async chatToken => {
	const chat = await users_db.getChat(chatToken)
	return chat
}

/**
 * Saves or unsaves an item for a specific user
 *
 * @param {*} request contains {token: string, save: boolean?, uid: string}
 * @returns {*} save object
 */
module.exports.saveUnsaveItem = async request => {
	const item = await this.extractSaveUnsaveItemData(request)
	const itemToReturn = await users_db.saveUnsaveItem(item.token, item.save, item.uid)
	return itemToReturn
}

/**
 * Send message to existing chat or creates new chat and sends the message to it
 *
 * @param {*} request contains {uidFrom, message, chatToken?, uidTo?, relatedItemToken?}
 * @returns {*} chat-message object
 */
module.exports.sendMessage = async request => {
	const message = await this.extractMessageData(request)
	const messageObject = await users_db.sendMessage(message.uidFrom, message.message, message.chatToken, message.uidTo, message.relatedItemToken)
	return messageObject
}

/**
 * Sets rating of a user for another user
 *
 * @param {*} request contains {uidFrom: string, uidTo: string, rating: int?}
 * @returns {*} rating object
 */
module.exports.setRating = async request => {
	const rating = await this.extractRatingData(request)
	const ratingToReturn = await users_db.setRating(rating.uidFrom, rating.uidTo, rating.rating)
	return ratingToReturn
}

/**
 * Saves a user report
 *
 * @param {*} request contains {uidReporter, uidReported, comment}
 * @returns {*} report object
 */
module.exports.reportUser = async request => {
	const report = await this.extractReportData(request)
	const reportToReturn = await users_db.reportUser(report)
	return reportToReturn
}

/**
 * Get an array of report tokens
 *
 * @returns {*} array of report tokens
 */
module.exports.getReportedUsers = async () => {
	const reports = await users_db.getReportedUsers()
	return reports
}

/**
 * Extracts report data from request object
 *
 * @param {uidReporter: string, uidReported: string, comment: string, timestmap: int} request
 */
module.exports.extractReportData = async request => ({
	uidReporter: request.body.uidReporter,
	uidReported: request.body.uidReported,
	comment: request.body.comment,
	timestmap: (new Date).getTime()
})

/**
 * Extracts rating data from request object
 *
 * @param {uidFrom: string, uidTo: string, rating: int} request
 */
module.exports.extractRatingData = async request => ({
	uidFrom: request.body.uidFrom,
	uidTo: request.body.uidTo,
	rating: request.body.rating
})

/**
 * Extracts message data from request object
 *
 * @param {uidFrom: string, message: string, chatToken: string?, uidTo: string?, relatedItemToken: string?} request
 */
module.exports.extractMessageData = async request => ({
	uidFrom: request.body.uidFrom,
	message: request.body.message,
	chatToken: request.body.chatToken ? request.body.chatToken : null,
	uidTo: request.body.uidTo ? request.body.uidTo : null,
	relatedItemToken: request.body.relatedItemToken ? request.body.relatedItemToken : null
})

/**
 * Extracts user data from request object
 *
 * @param {uid: string, username: string, email: string} request
 */
module.exports.extractUserData = async request => ({
	uid: request.body.uid,
	username: request.body.username,
	email: request.body.email
})

/**
 * Extracts save-unsave data from request object
 *
 * @param {token: string, save: bool?, uid: string} request
 */
module.exports.extractSaveUnsaveItemData = async request => ({
	token: request.body.token,
	save: request.body.save,
	uid: request.body.uid
})

