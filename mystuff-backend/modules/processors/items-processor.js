'use strict'

const items_db = require('../database/items-db')

/**
 * Adds an item for sale
 *
 * @param {*} request contains item object in body
 * @returns item object + token
 */
module.exports.createForSale = async request => {
	const item = await this.extractForSaleData(request)
	const itemFromDb = await items_db.createForSale(item)
	return itemFromDb
}

/**
 * Gets the conditions in database for an item
 *
 * @returns [string]
 */
module.exports.getItemConditions = async () => {
	const conditions = await items_db.getItemConditions()
	return conditions
}

/**
 * Gets the categories in database for an item
 *
 * @returns [string]
 */
module.exports.getItemCategories = async () => {
	const categories = await items_db.getItemCategories()
	return categories
}

/**
 * Gets items and filters them based on keywords if any passed
 *
 * @param {*} keywords [string] of keywords
 * @returns [item] array of item objects
 */
module.exports.getItems = async keywords => {
	const items = await items_db.getItems(keywords)
	return items
}

/**
 * Gets an item based on token
 *
 * @param {*} token item's unique token
 * @returns item object
 */
module.exports.getItem = async token => {
	const item = await items_db.getItem(token)
	return item
}

/**
 * Retrieves all items that a user added for sale
 *
 * @param {*} uid user authentication id
 * @returns [item] array of item objects
 */
module.exports.getUseritems = async uid => {
	const items = await items_db.getUseritems(uid)
	return items
}

/**
 * Sets an item to be published or unpublished (default is published)
 *
 * @param {*} request contains published object
 */
module.exports.setPublished = async request => {
	const publishedData = await this.extractPublishedData(request)
	await items_db.setPublished(publishedData.token, publishedData.published)
}

/**
 * Extracts item data from request object
 *
 * @param {category: string, city: string, condition: string, description: string, features: [stirng], images: [string], price: int, title: string, published: boolean, uid: string, timestamp: int} request
 */
module.exports.extractForSaleData = async request => ({
	category: request.body.category,
	city: request.body.city,
	condition: request.body.condition,
	description: request.body.description,
	features: request.body.features,
	images: request.body.images,
	price: request.body.price,
	title: request.body.title,
	published: true,
	uid: request.body.uid,
	timestamp: (new Date).getTime()
})

/**
 * Extracts published object data from request object
 *
 * @param {token: string, published: boolean} request
 */
module.exports.extractPublishedData = async request => ({
	token: request.body.token,
	published: request.body.published
})
